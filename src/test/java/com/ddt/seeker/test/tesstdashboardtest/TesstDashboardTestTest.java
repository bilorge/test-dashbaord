package com.ddt.seeker.test.tesstdashboardtest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author bilorge
 */
public class TesstDashboardTestTest {

    private TesstDashboardTest testee;

    public TesstDashboardTestTest() {
        testee = new TesstDashboardTest();
    }

    @org.junit.jupiter.api.Test
    public void testMain() {
        System.out.println("Testing the sayHello test.");
        assertEquals("Hello!", testee.sayHello());
    }

    @org.junit.jupiter.api.Test
    public void testMainNotInSpanish() {
        System.out.println("Testing the sayHello test to make sure its in English.");
        assertNotEquals("Hola!", testee.sayHello());
    }

}
